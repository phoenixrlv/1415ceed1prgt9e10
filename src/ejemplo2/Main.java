package ejemplo2;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.File;

public class Main {

    public static void main(String args[]) {

        ObjectContainer bd;
        String nombrefichero = "alumnos.db4o";
        File f = new File(nombrefichero);
        alumno a1, a2, a3, a4, a5, a6, a7;

        //f.delete();
        // Conectando a la BDOO
        bd = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(),
            nombrefichero);

        // Almacena Alumnos
        a1 = new alumno("Juan", 15, 12.5);
        bd.store(a1);
        a2 = new alumno("Vicente", 50, 20.1);
        bd.store(a2);
        System.out.println("Grabados Alumnos");

        System.out.println("Recuperado objetos alumno");
        a2 = new alumno(null, 0, 0);
        ObjectSet res = bd.queryByExample(a2);
        System.out.println("Objetos alumno recuperados: " + res.size());
        while (res.hasNext()) {
            System.out.println(res.next()); //toString
        }

        System.out.println("Recuperado objetos alumnos de edad 50");
        a3 = new alumno(null, 50, 0);
        res = bd.queryByExample(a3);
        System.out.println("Objetos alumno recuperados: " + res.size());
        while (res.hasNext()) {
            System.out.println(res.next()); //toString
        }

        System.out.println("Modificando Vicent");
        a4 = new alumno("Vicente", 0, 0);
        res = bd.queryByExample(a4);
        a5 = (alumno) res.next();
        a5.setNombre("Oscar");
        bd.store(a5);

        System.out.println("Borrando Juan");
        a6 = new alumno("Juan", 0, 0);
        res = bd.queryByExample(a6);
        a7 = (alumno) res.next();
        bd.delete(a7);

        boolean closed = bd.close();
    }
}
