package ejemplo2;

public class alumno {

    private String nombre;
    private int edad;
    private double nota;

    alumno(String nom, int e, double n) {

        nombre = nom;
        edad = e;
        nota = n;
    }

    public void setNombre(String n) {
        nombre = n;
    }

    public String toString() {
        return nombre + " " + edad + " " + nota;
    }

}
