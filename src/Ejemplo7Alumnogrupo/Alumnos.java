/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo7Alumnogrupo;

/**
 * @date 14-abr-2015 Fichero Alumnos.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Alumnos {

    private String idalumno;
    private String nombre;
    private Grupos grupo;

    Alumnos(String id_, String nombre_, Grupos grupo_) {
        idalumno = id_;
        nombre = nombre_;
        grupo = grupo_;
    }

    /**
     * @return the idalumno
     */
    public String getIdalumno() {
        return idalumno;
    }

    /**
     * @param idalumno the idalumno to set
     */
    public void setIdalumno(String idalumno) {
        this.idalumno = idalumno;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the grupo
     */
    public Grupos getGrupo() {
        return grupo;
    }

    /**
     * @param grupo the grupo to set
     */
    public void setGrupo(Grupos grupo) {
        this.grupo = grupo;
    }

    public String toString() {
        return idalumno + " " + nombre + " "
            + grupo;
    }
}
