/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo7Alumnogrupo;

/**
 * @date 14-abr-2015 Fichero Grupos.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Grupos {

    private String idgrupo;
    private String nombregrupo;

    Grupos(String idgrupo_, String nombregrupo_) {
        idgrupo = idgrupo_;
        nombregrupo = nombregrupo_;
    }

    /**
     * @return the idgrupo
     */
    public String getIdgrupo() {
        return idgrupo;
    }

    /**
     * @param idgrupo the idgrupo to set
     */
    public void setIdgrupo(String idgrupo) {
        this.idgrupo = idgrupo;
    }

    /**
     * @return the nombregrupo
     */
    public String getNombregrupo() {
        return nombregrupo;
    }

    /**
     * @param nombregrupo the nombregrupo to set
     */
    public void setNombregrupo(String nombregrupo) {
        this.nombregrupo = nombregrupo;
    }

    public String toString() {
        return idgrupo + " " + nombregrupo;
    }
}
